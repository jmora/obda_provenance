\section{Discussed approaches} \label{sec:approaches}
\contents{we provide a description of our three proposals, from a first approach where all the modifications are done in the mappings to a third approach that involves no modifications to the mappings}

In this section we provide a running example and present three different levels of granularity at which we can provide provenance annotations.
For a particular application, the expert responsible of modeling the domain can choose any of these levels of granularity or a combination of them
depending on the competency questions~\cite{suarez-figueroa2009} that need to be addressed by the OBDA system and the capabilities of the underlying system.
We extend PROV-O with additional predicates for convenience in the OBDA domain, we will refer to these predicates with the ``\obdaprov{}'' prefix.


\subsection{Running example}
For the running example we can consider data about movie ratings in a very simple scenario with two databases ($\data_1$ and $\data_2$), each one with one table.
Without loss of generality and for simplicity, both tables may have the same name (``movies'') and columns: \code{id}, \code{title}, \code{ageRating} and \code{score}.
We may be concerned about the provenance of the information for a variety of reasons, as already explained.
For example one of the reasons may be some disparity in the criteria for the age rating in different countries and thus databases, which may specialized in their own country and unaware of any other possible values.
In this particular example we only need one R2RML mapping assertion for each database\footnote{To keep the example simple and readable we formulate the mappings in a fashion similar to Direct Mapping, the URI templates do not conform to the best practices and we do not consider any links to state of the art ontologies.}.
The mapping for the first database may simply be:

\footnotesize\begin{verbatim}
:TriplesMap1
  a rr:TriplesMap ;
  rr:logicalTable [ rr:tableName "movies" ] ;
  rr:subjectMap [ 
    rr:template "http://movies.ex/db1/movie/{id}" ;
    rr:class movies:Movie ;
    rr:graph "http://movies.ex/db1/graph/movies"
  ] ;
  rr:predicateObjectMap [
    rr:predicate movies:hasTitle ;
    rr:objectMap [ rr:column "title" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate movies:hasAgeRating ;
    rr:objectMap [ rr:column "ageRating" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate movies:hasScore ;
    rr:objectMap [ rr:column "score" ]
  ] .
\end{verbatim}

The statement about the named graph is not necessary, but we will use it in \cref{sec:graphlvl}.
The mapping for the second database would be analogous.
For the sake of clarity, it may be noteworthy to point out that the reference to ``db1'' in the mappings is \emph{not} intended to provide provenance information, however, adding such a reference in the URI is a way to ensure the uniqueness of the URIs generated, since the ids are not guaranteed to be unique between both databases.

\subsection{Provenance at URI level}\label{sec:uriprovenance}

URIs identify the smallest fragments of information produced by OBDA systems.
To provide the provenance information we can simply state that the provenance of some URI corresponds to some \code{OBDAEntity}.
A mapping to add this kind of assertions would look like this:

\footnotesize\begin{verbatim}
:TriplesMap2
  a rr:TriplesMap ;
  rr:logicalTable [ rr:tableName "movies" ] ;
  rr:subjectMap [ 
    rr:template "http://movies.ex/db1/movie/{id}" ;
    rr:class prov:Entity
  ] ;
  rr:predicateObjectMap [
    rr:predicate prov:wasDerivedFrom ;
    rr:objectMap [
      rr:template "http://movies.ex/db1/OBDAURI/URI_movie_{id}"
    ]
  ] .
\end{verbatim}

With this mapping we add a new entity that is the source of the generated URIs, and we can generate all the provenance information for this entity with a new mapping: 

\footnotesize\begin{verbatim}
:TriplesMap3
  a rr:TriplesMap ;
  rr:logicalTable [ rr:tableName "movies" ] ;
  rr:subjectMap [ 
    rr:template "http://movies.ex/db1/OBDAURI/URI_movie_{id}" ;
    rr:class prov:Entity
  ] ;
  rr:predicateObjectMap [
    rr:predicate obdaprov:hasDatabase ;
    rr:objectMap [ rr:template "http://movies.ex/db1/database1" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate obdaprov:hasTable ;
    rr:objectMap [ rr:template "http://movies.ex/db1/movies" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate obdaprov:hasColumn ;
    rr:objectMap [ rr:template "http://movies.ex/db1/column_id" ]
  ] .
\end{verbatim}

It may be worthy to point out that several mappings can be responsible of the generation of the same URIs.
This is specially important if some join has to be performed between several data sources.
These URIs will be derived from several different entities, and the provenance will correspond to the union of the sources responsible for the generation of these URIs.

Additionally, note that \code{rr:template "http://movies.ex/db1/OBDAURI/URI\_movie\_{id}"} in \code{:TriplesMap2} could have been replaced with a \code{rr:parentTriplesMap} to \code{:TriplesMap3}.

\subsection{Provenance at triple level}\label{sec:inclprovenance}

Mapping assertions are of the form $\mappingassertion$.
To include the information about provenance in the mapping assertion we can include this information as constants in $\head$, the head of the mapping.
This can be done either manually or automatically if there is some automatic process for the generation of the mappings.
By using reification to keep track of this metainformation we can obtain the following result for the first \code{rr:predicateObjectMap}:

\footnotesize\begin{verbatim}
:TriplesMap2
  a rr:TriplesMap ;
  rr:logicalTable [ rr:tableName "movies" ] ;
  rr:subjectMap [ 
    rr:template "http://movies.ex/db1/statement/{id}" ;
    rr:class rdf:Statement ;
    rr:class prov:Entity
  ] ;
  rr:predicateObjectMap [
    rr:predicate rdf:subject ;
    rr:objectMap [
      rr:template "http://movies.ex/db1/movie/{id}"
    ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate rdf:predicate ;
    rr:object movies:hasTitle
  ] ;
  rr:predicateObjectMap [
    rr:predicate rdf:object ;
    rr:objectMap [ rr:column "title" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate obdaprov:wasDerivedWithOBDAMapping ;
    rr:object :TriplesMap1
  ] ;
  rr:predicateObjectMap [
    rr:predicate prov:wasDerivedFrom ;
    rr:object [
      rr:template "http://movies.ex/db1/OBDAEntity/URI_movie_title_{id}"
    ]
  ] .
\end{verbatim}

Adding two analogous mappings for the two remaining predicates in the original mapping and the corresponding mappings to describe the newly added resources (e.g. \code{"http://movies.ex/db1/OBDAEntity/URI\_movie\_title\_{id}"}), as in the previous example.
% In this solution we would need to add all the relevant triples about provenance in this kind of mapping assertions, as the last few \code{rr:predicateObjectMap} show.
% The specific triples about provenance to be added will depend on the use cases and competency questions that we want to be able to answer with the generated triples.
A new mapping (analogous to \code{:TriplesMap3} in the previous section) would be needed to specify the characteristics of \code{"http://movies.ex/db1/OBDAEntity/URI\_movie\_title\_{id}"}, i.e. its database, table and column.
All sorts of provenance information can be added to the triple reified in the previous map, for example the \prov{Activity} activity that generates it (\code{:ourOBDAActivity}) or the system (\code{:ourOBDASystem}) that as a \prov{SoftwareAgent} performs this activity.
This depends on the competency questions to be addressed for each particular use case.

%Of course we should add the model for the data sources, in this case the databases, the tables, etc. corresponding to URIs like \code{:ourOBDAActivity}.
%These assertions can be added as triples composed of constants in our OBDA system, either upgrading the TBox to a full ontology or linking to a separate file with this assertions.
%In our running example we should specify that \code{:ourOBDAActivity} is a \prov{Activity}, performed by our \code{:ourOBDASystem}, which is a \prov{SoftwareAgent}.
%Of course it is possible to add any other detail that may be relevant for any particular case, either referring to PROV-O or other ontologies.
%As usual, the model can be refined and improved by adding elements to it, for example we could define a subproperty of \prov{wasInfluencedBy} that would be specific to R2RML mapping assertions.

As a limitation, the mappings would generate additional triples due to reification, but the provenance from these triples cannot be offered with this solution due to the infinite regression that would imply.

\begin{comment}
\subsection{OBDA mappings imply provenance}

The previous approach uses reification, additional mappings for provenance and additional assertions in our ontology.
In this section we consider a less verbose approach.
We can modify the original mapping assertion by adding some blank nodes that replicate some information in the mapping, with this we can give access to the meta-information relative to the mapping along with the information in the database.
A possible approach could be extending the original mapping, obtaining the following:

\footnotesize\begin{verbatim}
:TriplesMap1
  a rr:TriplesMap ;
  rr:logicalTable [ rr:tableName "movies" ] ;
  rr:subjectMap [ 
    rr:template "http://movies.ex/db1/movie/{id}";
    rr:class movies:Movie
  ] ;
  rr:predicateObjectMap [
    rr:predicate movies:hasTitle ;
    rr:objectMap [ rr:column "title" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate movies:hasAgeRating ;
    rr:objectMap [ rr:column "ageRating" ]
  ] ;
  rr:predicateObjectMap [
    rr:predicate movies:hasScore ;
    rr:objectMap [ rr:column "score" ]
  ] ; 
  rr:predicateObjectMap [
    rr:predicate ext:hasTriplesMap ;
    rr:object [
      a ext:TriplesMapReflection ;
      ext:fromTriplesMap :TriplesMap1
      ext:hasPredicateObjectMap [
        ext:predicate movies:hasTitle ;
        rr:predicateObjectMap [
          rr:predicate ext:object ;
          rr:objectMap [ rr:column "title" ]
        ] ;
        ext:hasPredicateObjectMap [
        ext:predicate movies:hasTitle ;
        rr:predicateObjectMap [
          rr:predicate ext:object ;
          rr:objectMap [ rr:column "ageRating" ]
        ] ;
        ext:hasPredicateObjectMap [
        ext:predicate movies:hasTitle ;
        rr:predicateObjectMap [
          rr:predicate ext:object ;
          rr:objectMap [ rr:column "score" ]
        ] ;
      ]
    ]
  ] .
\end{verbatim}

The main difference with the previous approach is that no new mapping assertions are added, but the existing ones are extended.
We can infer the same result as in the previous approach with the axioms in Table~\ref{tab:axioms}
Please note that we use for the inference predicates in \code{ext:}, the extension of PROV-O for this purpose, defined in: \draft{I have to define it, it's basically what you could expect from the context}.

\begin{table*}
\newcommand{\simplifiedtablerow}[2]{$#1$&$\sqsubseteq$&$#2$\\}
\begin{tabular}{r c l}
	\simplifiedtablerow{\some{\inverseof{fromTriplesMap}}}{TriplesMap}
  \simplifiedtablerow{\some{\inverseof{fromTriplesMap}}}{\some[Triple]{\inverseof{wasInfluencedBy}}}
  \simplifiedtablerow{\propertychain{\propertychain{wasInfluencedBy}{\inverseof{fromTriplesMap}}}{hasTriplesMap}}{hasSubject}
  \simplifiedtablerow{\propertychain{\propertychain{wasInfluencedBy}{\inverseof{fromTriplesMap}}}{\propertychain{hasPredicateObject}{predicate}}}{hasPredicate}
  \simplifiedtablerow{\propertychain{\propertychain{wasInfluencedBy}{\inverseof{fromTriplesMap}}}{\propertychain{hasPredicateObject}{object}}}{hasObject}
  \simplifiedtablerow{\propertychain{wasInfluencedBy}{\propertychain{\inverseof{fromTriplesMap}}{mapsTable}}}{prov:wasDerivedFrom}
  \simplifiedtablerow{\propertychain{wasInfluencedBy}{\propertychain{\inverseof{fromTriplesMap}}{mapsTable}}}{prov:wasGeneratedBy}
\end{tabular}
\label{tab:axioms}
\caption{axioms}
\end{table*}


This approach provides access to provenance information by duplicating the meta-information about the mappings so that this meta-information is included with the regular information, produced by the mappings.
\end{comment}

\subsection{Provenance at graph level}\label{sec:graphlvl}

For many applications the triple granularity will not be needed.
If a set of triples share some details about their provenance (e.g. the source database is the same) then a more efficient solution can be annotating provenance for all of them together in a RDF named graph.
This can easily be done by modifying the original mappings and specifying a graph containing the triples that share some provenance detail and creating additional mappings to state the provenance of that graph.
The statements about the provenance can be included in the same graph, allowing for a closed recursion.

If graphs refer to greater entities (e.g. data sources) their specification will very probably be more stable.
Adding a new graph with details about provenance may be an usual operation that can be performed manually (or semi-automatically) when adding a new data source to the system.
Therefore, the information about the provenance of the graph can be stated statically without requiring additional mappings for this, for example:

\footnotesize\begin{verbatim}
<http://movies.ex/db1/graph/movies>
  a Entity ;
  prov:wasDerivedFrom "http://movies.ex/db1/OBDAEntity/database_db1" .
\end{verbatim}

As in previous examples, the properties of \code{"http://movies.ex/db1/OBDAEntity/table\_movies"} would be defined in a new map specifying the original database and any other details to consider.