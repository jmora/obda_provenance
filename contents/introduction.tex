\section{Introduction} \label{sec:introduction}

Ontology-Based Data Access (OBDA) enables the access to information stored in different types of data sources, normally databases, and provides an interface to that information that corresponds to an ontology, usually a TBox~\cite{poggi2008}.
The ontology is used as the interface for the queries to the system, providing inference capabilities and an abstraction from the details of the data sources, i.e. users can query OBDA systems as they would query ontologies and the extraction of the information from the data sources is transparent to the users.

In the past, the work in  OBDA  has focused on providing access to the information that is stored in the data sources by means of queries, i.e. query answering.
These queries can be answered by posing them over a materialization of the data expressed according to the ontology, for instance one stored in RDF, or by rewriting user queries into queries that can be posed to the data sources.
In either case, the relationship between the ontology and the data sources is made explicit in the form of a set of mappings.
To the best of our knowledge, these mappings provide access exclusively to the information stored in the data source, possibly adding a set of constants in each of them.
However, specially when integrating information from several different data sources, users may be concerned with some meta-information that is not captured by the mappings.

In particular, the abstraction from the data sources acts as a wall that prevents obtaining information about the original provenance of the data.
Nevertheless, this provenance and the traceability it provides can be useful for a number of reasons.
We can consider several examples:

\begin{itemize}
	\item debugging a set of mappings, pinpointing the relevant mapping assertions at each time.
  \item finding the original source of information to correct possible errors in it, e.g. replacing faulty sensors that are producing incorrect data.
  \item finding some other meta-information about the data, in case the meta-information is related to the origin of the data, e.g. trust information or quality of the data (or the data source).
\end{itemize}

\begin{comment}
Among the types of meta-information that may be relevant for a user we find many different possible measures, for example we can consider:
\begin{description}
  \item[quality] e.g. accuracy, timeliness, precision, cost, etc.
    Quality meta-information may be relevant when deciding which data sources should be queried and how the information should be processed after being obtained.
  \item[trust] which will depend on several factors, for example authorship.
    Trust meta-information may be relevant for users that need to make decisions based on the information that they obtain from the OBDA system.
  \item[provenance] which provides information traceability and has a special relevance due to its relation on other types of meta-information.
    This can allow the identification of the origin of the data and taking actions on it, e.g. replacing a sensor that is producing faulty data.
\end{description}
\end{comment}

In this paper we study the possibility of enabling the access to provenance metadata in OBDA systems, as a starting point to enable the access to other types of meta-information.
Among the different types of metadata, provenance provides traceability capabilities for the information obtained from the data sources, potentially up to their origin.
With these traceability capabilities, other metadata could possibly be queried, calculated or estimated from the data sources, after their identification has been enabled.
For this reason, we focus on provenance information as an entry point, enabler and facilitator for the access to other types of meta-information related with the data sources.
% Up to date, OBDA systems give access to the information stored in the databases, possibly adding some constants related with the knowledge that the domain expert provides for the creation of the mappings.
% However there is no access to meta-information and this is a conceptual and qualitative difference to consider and describe.

For practical reasons and without loss of generality, we focus on two W3C standards for OBDA mappings and provenance, respectively R2RML~\cite{das2012} and PROV-O~\cite{belhajjame2012}. 
We analyse the problem of provenance in OBDA systems considering several different options:
\begin{itemize}
	\item In terms of the granularity, the provenance can refer to specific URIs, individual triples or the provenance characteristics shared by the triples in a named graph (e.g. all come from the same database).
  \item We consider extending PROV-O to address in a more specific and concise way the characteristics of OBDA systems.
	\item Implementation wise, the provenance metadata can be provided with explicit additional mappings or in a transparent way by a provenance-aware OBDA system. We analyse these possibilities.
\end{itemize}

Some other problems may arise in the intersection of OBDA systems and provenance.
To avoid confusion and in the sake of clarity we describe and analyse the two most immediate problems in this intersection:
\begin{enumerate}
  \item Modeling the provenance of elements in a OBDA specification, e.g. OBDA mappings.
  \item Providing access from the ontology to provenance data stored in databases.
\end{enumerate}

\listofstructure{}
